# plugsurfing-coding-challenge-php
1. Write a function insertAd(array $arg) that takes array as an argument and returns an array with "advertisement" inserted after each 5th element.

        ex: $arg = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        $result = [1, 2, 3, 4, 5, 'advertisement', 6, 7, 8, 9, 10, 'advertisement', 11, 12]

2. Write a function sortPairs(array $pairs) that takes array of pairs of integers (i,j) as an argument and returns a "sorted" collection by first element in pair (i)

        ex: pairs = [[1,4], [6,8], [0,0], [4, 2]]
        result = [[0,0], [1, 4], [4,2], [6,8]]

3. Implement a simple application "Event manager":

    1. Main page should contain a list of "events" of two types "Call" and "Meeting":
        1. user can create/edit/delete event;
        2. events must be ordered by event date (desc);
        3. all events fields should be shown on the main page.

    2. User can create/delete/edit event "Call":
        1. fields: event_date (datetime), name (string), created_date (date), exactly 2 participants;
        2. participant should provide a valid email;
        3. when new "Call" type event is created an email about it should be sent to participants.

    3. User can create/delete/edit event "Meeting":
        1. fields: event_date (date), name (string), created_date (date), exactly 3 participants, address (string);
        2. participant is just a name;
        3. clicking on the address (main page) should open Google Maps.

Your pull request should contain:

* High quality, well documented and tested code following clean architecture principles;
* Instructions for our QA (not PHP engineer!), guiding how to setup and run "Event manager" application in a safe and emulated environment on Mac.

Good luck!
