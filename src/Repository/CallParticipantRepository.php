<?php

namespace App\Repository;

use App\Entity\CallParticipant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CallParticipant|null find($id, $lockMode = null, $lockVersion = null)
 * @method CallParticipant|null findOneBy(array $criteria, array $orderBy = null)
 * @method CallParticipant[]    findAll()
 * @method CallParticipant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CallParticipantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CallParticipant::class);
    }

    // /**
    //  * @return CallParticipant[] Returns an array of CallParticipant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CallParticipant
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
