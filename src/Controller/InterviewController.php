<?php
/**
 * Created by PhpStorm.
 * User: waheedo
 * Date: 09.10.19
 * Time: 14:21
 */

namespace App\Controller;

use function MongoDB\BSON\toJSON;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InterviewController extends AbstractController
{

    /**
     * @Route("/first")
     */
    public function addAd() {
        $arg = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $temp = [];
        $wordToAppend = 'advertisement';
        foreach($arg as $k=>$v) {
            if($k%5 == 0 && $k != 0) {
                $temp[] = $wordToAppend;
            } else {
                $temp[] = $arg[$k];
            }
        }

        return $this->render('interview/show.html.twig', [
            'content'  => $temp
        ]);

    }

    /**
     * @Route("/second")
    */
    public function sortPairs() {
        $pairs = [[1,4], [6,8], [0,0], [4, 2],[4,3]];
        $result = [];
        $temp = [];
        foreach($pairs as $k=>$v) {
                $temp[$v[0]][] = $v;
        }

        ksort($temp);
        foreach($temp as $k =>$v) {
            foreach($v as $subArray) {
                $result[] = $subArray;
            }
        }

        return $this->render('interview/show.html.twig', [
            'content'  => $result
        ]);
    }


//    /**
//     * @Route("/")
//    */
//    public function index(LoggerInterface $logger, AdapterInterface $cache) {
////        $logger->info('I just got the logger');
////        $logger->error('An error occurred');
//
//        $str = "Hello Newman!"; // heavy processing on this string? cache the result, so you don't repeat the processing again.
//
//        $item = $cache->getItem('message_'.md5($str));
//        if(!$item->isHit()) {
//            $item->set($str); //markdown e.g
//            $cache->save($item);
//        }
//
//        $str = $item->get();
//
//        return new Response($str);
//    }
//
//    /**
//     * @Route("news/{slug}", name="showNews")
//    */
//    public function show($slug) {
//        //return new Response(sprintf("Article of Slug %s", $slug));
//
//        $comments = ["Comment one", "Comment two", "Comment three"];
//
//        //dump($slug, $this);
//        return $this->render('interview/show.html.twig', [
//            'title'     => ucwords((str_replace('-', ' ', $slug))),
//            'comments'  => $comments
//        ]);
//    }
//
//
//    /**
//     * @Route("/news/{slug}/heart", name="like_article", methods={"POST"})
//    */
//    public function likeArticle($slug) {
//        //
//        return new JsonResponse(['count' => rand(4, 10)]);
//    }
}