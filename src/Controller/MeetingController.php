<?php

namespace App\Controller;

use App\Entity\Meeting;
use Doctrine\DBAL\Types\TextType;
use http\Env\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MeetingController extends AbstractController
{
    /**
     * @Route("/meeting", name="meeting")
     */
    public function index()
    {
        $meetings = $this->getDoctrine()
            ->getRepository(Meeting::class)
            ->findAll([], []);
        return $this->render('meeting/index.html.twig', [
            'controller_name' => 'MeetingController',
            'meetings' => $meetings
        ]);
    }

    /**
     * @Route("/meeting/{id}", name="single_meeting")
     */
    public function show($id){
        $meeting = $this->getDoctrine()
            ->getRepository(Meeting::class)
            ->find($id);

        return $this->render('meeting/show.html.twig', ['m' =>$meeting]);
    }


    /**
     * @Route ("/meeting/new", name="new_meeting")
     * Method ({"GET", "POST"})
     */
    public function new(\Symfony\Component\HttpFoundation\Request $request) {
        $meeting = new Meeting();
        $form = $this->createFormBuilder($meeting)
            ->add('name', TextType::class, array('attr' =>array('class' => 'form-control')))
            ->add('eventDate', TextType::class, array('attr' =>array('class' => 'form-control')))
            ->add('createDate', TextType::class, array('attr' =>array('class' => 'form-control')))
            ->add('address', TextType::class, array('attr' =>array('class' => 'form-control')))
            ->add('save', SubmitType::class, array(
                'label' =>'Create',
                'attr' =>array('class'=>'btn btn-primary mt-3')
            ))
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $meeting = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($meeting);
            $entityManager->flush();
            return $this->redirectToRoute('meeting');
        }
        return $this->render('meeting/new.html.twig',array(
            'form'=>$form->createView()
        ));
    }

}
