<?php

namespace App\Controller;

use App\Entity\Call;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CallController extends AbstractController
{
    /**
     * @Route("/call", name="call")
     */
    public function index()
    {
        $calls = $this->getDoctrine()
            ->getRepository(Call::class)
            ->findAll();
        return $this->render('call/index.html.twig', [
            'controller_name' => 'CallController',
            'calls' => $calls
        ]);
    }

}
