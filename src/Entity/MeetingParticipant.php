<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MeetingParticipantRepository")
 */
class MeetingParticipant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Meeting", inversedBy="yes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $meeting_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMeetingId(): ?Meeting
    {
        return $this->meeting_id;
    }

    public function setMeetingId(?Meeting $meeting_id): self
    {
        $this->meeting_id = $meeting_id;

        return $this;
    }
}
