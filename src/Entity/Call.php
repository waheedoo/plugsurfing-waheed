<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CallRepository")
 */
class Call
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $event_date;

    /**
     * @ORM\Column(type="date")
     */
    private $created_date;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CallParticipant", mappedBy="call_id", orphanRemoval=true)
     */
    private $callParticipants;

    public function __construct()
    {
        $this->callParticipants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEventDate(): ?\DateTimeInterface
    {
        return $this->event_date;
    }

    public function setEventDate(\DateTimeInterface $event_date): self
    {
        $this->event_date = $event_date;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->created_date;
    }

    public function setCreatedDate(\DateTimeInterface $created_date): self
    {
        $this->created_date = $created_date;

        return $this;
    }

    /**
     * @return Collection|CallParticipant[]
     */
    public function getCallParticipants(): Collection
    {
        return $this->callParticipants;
    }

    public function addCallParticipants(CallParticipant $callParticipant): self
    {
        if (!$this->callParticipants->contains($callParticipant)) {
            $this->callParticipants[] = $callParticipant;
            $callParticipant->setCallId($this);
        }

        return $this;
    }

    public function removeCallParticipants(CallParticipant $callParticipant): self
    {
        if ($this->callParticipants->contains($callParticipant)) {
            $this->callParticipants->removeElement($callParticipant);
            // set the owning side to null (unless already changed)
            if ($callParticipant->getCallId() === $this) {
                $callParticipant->setCallId(null);
            }
        }

        return $this;
    }
}
