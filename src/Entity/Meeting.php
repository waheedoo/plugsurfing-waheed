<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MeetingRepository")
 */
class Meeting
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $event_date;

    /**
     * @ORM\Column(type="date")
     */
    private $created_date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MeetingParticipant", mappedBy="meeting_id", orphanRemoval=true)
     */
    private $meetingParticipants;

    public function __construct()
    {
        $this->meetingParticipants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEventDate(): ?\DateTimeInterface
    {
        return $this->event_date;
    }

    public function setEventDate(\DateTimeInterface $event_date): self
    {
        $this->event_date = $event_date;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->created_date;
    }

    public function setCreatedDate(\DateTimeInterface $created_date): self
    {
        $this->created_date = $created_date;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection|MeetingParticipant[]
     */
    public function getMeetingParticipants(): Collection
    {
        return $this->meetingParticipants;
    }

    public function addYe(MeetingParticipant $meetingParticipant): self
    {
        if (!$this->meetingParticipants->contains($meetingParticipant)) {
            $this->meetingParticipants[] = $meetingParticipant;
            $meetingParticipant->setMeetingId($this);
        }

        return $this;
    }

    public function removeMeetingParticipant(MeetingParticipant $meetingParticipant): self
    {
        if ($this->meetingParticipants->contains($meetingParticipant)) {
            $this->meetingParticipants->removeElement($meetingParticipant);
            // set the owning side to null (unless already changed)
            if ($meetingParticipant->getMeetingId() === $this) {
                $meetingParticipant->setMeetingId(null);
            }
        }

        return $this;
    }
}
