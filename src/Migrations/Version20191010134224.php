<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191010134224 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE meeting (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, event_date DATE NOT NULL, created_date DATE NOT NULL, address VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE call_participant (id INT AUTO_INCREMENT NOT NULL, call_id_id INT NOT NULL, email VARCHAR(255) NOT NULL, INDEX IDX_C7618E0A1DAE02EF (call_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE meeting_participant (id INT AUTO_INCREMENT NOT NULL, meeting_id_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_FBFF65641775DC57 (meeting_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `call` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, event_date DATETIME NOT NULL, created_date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE call_participant ADD CONSTRAINT FK_C7618E0A1DAE02EF FOREIGN KEY (call_id_id) REFERENCES `call` (id)');
        $this->addSql('ALTER TABLE meeting_participant ADD CONSTRAINT FK_FBFF65641775DC57 FOREIGN KEY (meeting_id_id) REFERENCES meeting (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE meeting_participant DROP FOREIGN KEY FK_FBFF65641775DC57');
        $this->addSql('ALTER TABLE call_participant DROP FOREIGN KEY FK_C7618E0A1DAE02EF');
        $this->addSql('DROP TABLE meeting');
        $this->addSql('DROP TABLE call_participant');
        $this->addSql('DROP TABLE meeting_participant');
        $this->addSql('DROP TABLE `call`');
    }
}
